package dev.fizcode.tokopaerbe_compose

import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.navigation.compose.rememberNavController
import dagger.hilt.android.AndroidEntryPoint
import dev.fizcode.tokopaerbe_compose.ui.navigation.navgraph.RootNavigationGraph
import dev.fizcode.tokopaerbe_compose.ui.theme.TokopaerbeComposeTheme

@AndroidEntryPoint
class MainActivity : ComponentActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            TokopaerbeComposeTheme {
                RootNavigationGraph(navController = rememberNavController())
            }
        }
    }
}