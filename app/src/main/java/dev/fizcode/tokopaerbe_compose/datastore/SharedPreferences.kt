package dev.fizcode.tokopaerbe_compose.datastore

import android.content.Context
import androidx.datastore.core.DataStore
import androidx.datastore.preferences.SharedPreferencesMigration
import androidx.datastore.preferences.core.Preferences
import androidx.datastore.preferences.core.edit
import androidx.datastore.preferences.preferencesDataStore
import dev.fizcode.tokopaerbe_compose.common.Constant
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.map
import javax.inject.Inject

/**
 * Created By FizCode on 1/20/2024.
 */
class SharedPreferences @Inject constructor(private val context: Context) {

    companion object {

        val Context.datastore: DataStore<Preferences> by preferencesDataStore(
            name = Constant.PrefDatastore.PREF_NAME,
            produceMigrations = ::sharedPreferenceMigration
        )

        private fun sharedPreferenceMigration(context: Context) =
            listOf(SharedPreferencesMigration(context, Constant.PrefDatastore.PREF_NAME))
    }

    suspend fun getOnBoarding(): Flow<String> {
        return context.datastore.data.map { preferences ->
            preferences[Constant.PrefDatastore.ONBOARDING].orEmpty()
        }
    }

    suspend fun setOnBoarding(value: String) {
        context.datastore.edit { preferences ->
            preferences[Constant.PrefDatastore.ONBOARDING] = value
        }
    }

    suspend fun getToken(): Flow<String> {
        return context.datastore.data.map { preference ->
            preference[Constant.PrefDatastore.ACCESS_TOKEN].orEmpty()
        }
    }

    suspend fun setToken(value: String) {
        context.datastore.edit { prefernces ->
            prefernces[Constant.PrefDatastore.ACCESS_TOKEN] = value
        }
    }
}