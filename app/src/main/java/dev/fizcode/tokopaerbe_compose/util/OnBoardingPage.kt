package dev.fizcode.tokopaerbe_compose.util

import androidx.annotation.DrawableRes
import dev.fizcode.tokopaerbe_compose.R

/**
 * Created By FizCode on 1/20/2024.
 */
sealed class OnBoardingPage (
    @DrawableRes
    val image: Int
) {
    object First: OnBoardingPage(
        image = R.drawable.onboard1
    )

    object Second: OnBoardingPage(
        image = R.drawable.onboard2
    )

    object Third: OnBoardingPage(
        image = R.drawable.onboard3
    )
}