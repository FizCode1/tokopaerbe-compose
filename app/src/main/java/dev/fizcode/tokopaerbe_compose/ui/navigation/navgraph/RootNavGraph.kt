package dev.fizcode.tokopaerbe_compose.ui.navigation.navgraph

import androidx.compose.runtime.Composable
import androidx.navigation.NavHostController
import androidx.navigation.compose.NavHost
import androidx.navigation.compose.composable
import dev.fizcode.tokopaerbe_compose.ui.navigation.navigationbar.MainMenuScreen

/**
 * Created By FizCode on 1/20/2024.
 */

@Composable
fun RootNavigationGraph(navController: NavHostController) {
    NavHost(
        navController = navController,
        route = Graph.ROOT,
        startDestination = Graph.SPLASH
    ) {
        splashNavGraph(navHostController = navController)
        onBoardingNavGraph(navController = navController)
        authNavGraph(navController = navController)
        composable(route = Graph.MAIN_MENU) {
            MainMenuScreen()
        }
    }
}

object Graph {
    const val ROOT = "root_graph"
    const val SPLASH = "splash_graph"
    const val ONBOARDING = "onboarding_graph"
    const val AUTHENTICATION = "auth_graph"
    const val MAIN_MENU = "main_menu_graph"
    const val DETAILS = "details_graph"
}