package dev.fizcode.tokopaerbe_compose.ui.navigation.navgraph

import androidx.compose.foundation.layout.PaddingValues
import androidx.compose.runtime.Composable
import androidx.navigation.NavHostController
import androidx.navigation.compose.NavHost
import androidx.navigation.compose.composable
import dev.fizcode.tokopaerbe_compose.ui.navigation.navigationbar.BottomBarItems
import dev.fizcode.tokopaerbe_compose.ui.screens.mainmenu.home.HomeScreen
import dev.fizcode.tokopaerbe_compose.ui.screens.mainmenu.store.StoreScreen
import dev.fizcode.tokopaerbe_compose.ui.screens.mainmenu.transaction.TransactionScreen
import dev.fizcode.tokopaerbe_compose.ui.screens.mainmenu.wishlist.WishlistScreen

/**
 * Created By FizCode on 1/21/2024.
 */

@Composable
fun MainMenuNavGraph(navController: NavHostController, innerPadding: PaddingValues) {
    NavHost(
        navController = navController,
        route = Graph.MAIN_MENU,
        startDestination = BottomBarItems.Home.route
    ) {
        composable(route = BottomBarItems.Home.route) {
            HomeScreen()
        }
        composable(route = BottomBarItems.Store.route) {
            StoreScreen()
        }
        composable(route = BottomBarItems.Wishlist.route) {
            WishlistScreen()
        }
        composable(route = BottomBarItems.Transaction.route) {
            TransactionScreen()
        }
    }
}