package dev.fizcode.tokopaerbe_compose.ui.navigation.navigationbar

import androidx.compose.foundation.layout.RowScope
import androidx.compose.material3.Icon
import androidx.compose.material3.NavigationBar
import androidx.compose.material3.NavigationBarItem
import androidx.compose.material3.Scaffold
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.navigation.NavDestination
import androidx.navigation.NavDestination.Companion.hierarchy
import androidx.navigation.NavGraph.Companion.findStartDestination
import androidx.navigation.NavHostController
import androidx.navigation.compose.currentBackStackEntryAsState
import androidx.navigation.compose.rememberNavController
import dev.fizcode.tokopaerbe_compose.ui.navigation.navgraph.MainMenuNavGraph

/**
 * Created By FizCode on 1/21/2024.
 */

@Composable
fun MainMenuScreen(navController: NavHostController = rememberNavController()) {
    Scaffold(
        bottomBar = { NavigationBarScreen(navController = navController) }
    ) { innerPadding ->
        MainMenuNavGraph(navController = navController, innerPadding = innerPadding)
    }
}

@Composable
fun NavigationBarScreen(navController: NavHostController) {
    val screens = listOf(
        BottomBarItems.Home,
        BottomBarItems.Store,
        BottomBarItems.Wishlist,
        BottomBarItems.Transaction
    )
    val navBackStackEntry by navController.currentBackStackEntryAsState()
    val currentDestination = navBackStackEntry?.destination

    NavigationBar {
        screens.forEach { screen ->
            AddItem(
                screen = screen,
                currentDestination = currentDestination,
                navController = navController
            )
        }
    }
}

@Composable
fun RowScope.AddItem(
    screen: BottomBarItems,
    currentDestination: NavDestination?,
    navController: NavHostController
) {
    NavigationBarItem(
        label = { Text(text = screen.title) },
        icon = { Icon(imageVector = screen.icon, contentDescription = "Navigation Icon") },
        selected = currentDestination?.hierarchy?.any {
            it.route == screen.route
        } == true,
        onClick = {
            navController.navigate(screen.route) {
                popUpTo(navController.graph.findStartDestination().id)
                launchSingleTop = true
            }
        }
    )
}
