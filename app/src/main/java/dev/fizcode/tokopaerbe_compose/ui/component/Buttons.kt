package dev.fizcode.tokopaerbe_compose.ui.component

import androidx.compose.material3.Button
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.OutlinedButton
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.text.TextStyle

/**
 * Created By FizCode on 1/20/2024.
 */

@Composable
fun PrimaryButton(
    modifier: Modifier,
    text: String,
    textStyle: TextStyle = MaterialTheme.typography.labelLarge,
    onClick: () -> Unit
) {
    Button(
        modifier = modifier,
        onClick = onClick,
    ) {
        Text(
            style = textStyle,
            text = text
        )
    }
}

@Composable
fun SecondaryButton(
    modifier: Modifier,
    text: String,
    textStyle: TextStyle = MaterialTheme.typography.labelLarge,
    onClick: () -> Unit
) {
    OutlinedButton(
        modifier = modifier,
        onClick = onClick
    ) {
        Text(
            style = textStyle,
            text = text
        )
    }
}