package dev.fizcode.tokopaerbe_compose.ui.screens.splash

import androidx.compose.runtime.MutableState
import androidx.compose.runtime.mutableStateOf
import androidx.lifecycle.ViewModel
import dagger.hilt.android.lifecycle.HiltViewModel
import dev.fizcode.tokopaerbe_compose.common.Constant
import dev.fizcode.tokopaerbe_compose.repository.AuthRepository
import dev.fizcode.tokopaerbe_compose.repository.OnBoardingRepository
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import javax.inject.Inject

/**
 * Created By FizCode on 1/20/2024.
 */

@HiltViewModel
class SplashViewModel @Inject constructor(
    private val onBoardingRepository: OnBoardingRepository,
    private val authRepository: AuthRepository
): ViewModel() {

    val shouldSkipOnBoarding: MutableState<Boolean> = mutableStateOf(false)
    val shouldSignIn: MutableState<Boolean> = mutableStateOf(false)


    fun onViewLoaded() {
        getOnBoardingValue()
    }

    private fun getOnBoardingValue() {
        CoroutineScope(Dispatchers.IO).launch {
            val response = onBoardingRepository.getOnboarding()
            withContext(Dispatchers.Main) {
                shouldSkipOnBoarding.value = response == Constant.PrefValueSetter.ONBOARDING_STATUS
            }
        }
    }

    private fun getSingInValue() {
        CoroutineScope(Dispatchers.IO).launch {
            val response = authRepository.getToken()
            withContext(Dispatchers.Main) {
                shouldSignIn.value = response.isNullOrEmpty()
            }
        }
    }
}