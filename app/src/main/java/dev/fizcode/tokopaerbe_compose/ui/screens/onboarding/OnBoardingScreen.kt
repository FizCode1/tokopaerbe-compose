package dev.fizcode.tokopaerbe_compose.ui.screens.onboarding

import androidx.compose.foundation.ExperimentalFoundationApi
import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.pager.HorizontalPager
import androidx.compose.foundation.pager.PagerState
import androidx.compose.foundation.pager.rememberPagerState
import androidx.compose.foundation.shape.CircleShape
import androidx.compose.material3.Button
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Text
import androidx.compose.material3.TextButton
import androidx.compose.runtime.Composable
import androidx.compose.runtime.rememberCoroutineScope
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.alpha
import androidx.compose.ui.draw.clip
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import androidx.hilt.navigation.compose.hiltViewModel
import androidx.navigation.NavHostController
import dev.fizcode.tokopaerbe_compose.ui.component.WhiteSystemBar
import dev.fizcode.tokopaerbe_compose.ui.navigation.navgraph.AuthScreen
import dev.fizcode.tokopaerbe_compose.ui.navigation.navgraph.Graph
import dev.fizcode.tokopaerbe_compose.util.OnBoardingPage
import kotlinx.coroutines.launch

/**
 * Created By FizCode on 1/20/2024.
 */

@Composable
fun OnBoardingScreen(navController: NavHostController) {

    // Change System Bar Background to be White
    WhiteSystemBar()

    // BindViewModel
    val onBoardingViewModel: OnBoardingViewModel = hiltViewModel()

    // Bind View
    val onClickSkip = {
        onBoardingViewModel.completeOnboarding()
        navController.popBackStack()
        navController.navigate(Graph.AUTHENTICATION)
        println("Clicked: OnClick Skip")
    }
    val onClickJoin = {
        onBoardingViewModel.completeOnboarding()
        navController.popBackStack()
        navController.navigate(AuthScreen.SignUp.route)
        println("Clicked: OnClick Join")
    }

    OnBoardingUI(onClickJoin = onClickJoin, onClickSkip = onClickSkip)
}

@OptIn(ExperimentalFoundationApi::class)
@Composable
private fun OnBoardingUI(
    onClickSkip: () -> Unit,
    onClickJoin: () -> Unit
) {

    val page = listOf(
        OnBoardingPage.First,
        OnBoardingPage.Second,
        OnBoardingPage.Third
    )

    val scope = rememberCoroutineScope()
    val pagerState = rememberPagerState(pageCount = { 3 })
    val buttonAlpha = if (pagerState.currentPage == 2) 0f else 1f

    Column(
        modifier = Modifier.fillMaxSize()
    ) {
        HorizontalPager(
            modifier = Modifier.weight(1f),
            state = pagerState
        ) { position ->

            // Pager Screen
            PagerScreen(onBoardingPage = page[position])
        }
        Column(
            modifier = Modifier.fillMaxWidth(),
            horizontalAlignment = Alignment.CenterHorizontally,
            verticalArrangement = Arrangement.Bottom
        ) {

            // Navigate to Sign Up and set onboarding as already opened
            Button(
                modifier = Modifier
                    .fillMaxWidth()
                    .padding(horizontal = 16.dp),
                onClick = onClickJoin
            ) {
                Text(text = "Gabung Sekarang")
            }

            Row(
                modifier = Modifier
                    .padding(horizontal = 16.dp)
                    .fillMaxWidth()
            ) {

                // Navigate to Sign In and set onboarding as already opened
                TextButton(
                    modifier = Modifier
                        .weight(1f),
                    onClick = onClickSkip
                ) {
                    Text(
                        modifier = Modifier.fillMaxWidth(),
                        text = "Lewati"
                    )
                }

                // Pager position with Dot Indicator
                Row(
                    modifier = Modifier
                        .weight(1f)
                        .padding(16.dp),
                    horizontalArrangement = Arrangement.Center
                ) {
                    PagerPositionState(pagerState = pagerState)
                }

                TextButton(
                    modifier = Modifier
                        .weight(1f)
                        .alpha(buttonAlpha),
                    onClick = {
                        if (pagerState.currentPage < 3) {
                            scope.launch {
                                pagerState.scrollToPage(pagerState.currentPage + 1)
                            }
                        }
                    }
                ) {
                    Text(
                        modifier = Modifier.fillMaxWidth(),
                        textAlign = TextAlign.End,
                        text = "Selanjutnya"
                    )
                }
            }
        }
    }
}

@OptIn(ExperimentalFoundationApi::class)
@Composable
private fun PagerPositionState(pagerState: PagerState) {
    repeat(pagerState.pageCount) { itertation ->
        val color =
            if (pagerState.currentPage == itertation) MaterialTheme.colorScheme.primary
            else MaterialTheme.colorScheme.primaryContainer
        Box(
            modifier = Modifier
                .padding(8.dp)
                .clip(CircleShape)
                .background(color)
                .size(8.dp)
        )
    }
}

@Composable
private fun PagerScreen(
    onBoardingPage: OnBoardingPage,
) {
    Image(
        modifier = Modifier.fillMaxWidth(),
        painter = painterResource(id = onBoardingPage.image),
        contentScale = ContentScale.FillWidth,
        contentDescription = "OnBoarding Image"
    )
}