package dev.fizcode.tokopaerbe_compose.ui.component

import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.text.KeyboardOptions
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Visibility
import androidx.compose.material.icons.filled.VisibilityOff
import androidx.compose.material3.Icon
import androidx.compose.material3.IconButton
import androidx.compose.material3.OutlinedTextField
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Modifier
import androidx.compose.ui.text.input.KeyboardType
import androidx.compose.ui.text.input.PasswordVisualTransformation
import androidx.compose.ui.text.input.VisualTransformation

/**
 * Created By FizCode on 1/20/2024.
 */

@Composable
fun EmailTextField(
    value: String,
    onValueChanged: (String) -> Unit,
    isError: Boolean,
    label: String,
    placeholder: String,
    supportingText: String,
) {
    OutlinedTextField(
        modifier = Modifier
            .fillMaxWidth(),
        singleLine = true,
        keyboardOptions = KeyboardOptions(keyboardType = KeyboardType.Email),
        value = value,
        onValueChange = onValueChanged,
        isError = isError,
        placeholder = { Text(text = placeholder) },
        label = { Text(text = label) },
        supportingText = { Text(text = supportingText) }
    )
}

@Composable
fun PasswordTextField(
    value: String,
    onValueChanged: (String) -> Unit,
    label: String,
    placeholder: String,
    isError: Boolean,
    supportingText: String
) {
    var showPassword by remember { mutableStateOf(value = false) }
    OutlinedTextField(
        value = value,
        onValueChange = onValueChanged,
        modifier = Modifier.fillMaxWidth(),
        trailingIcon = {
            if (showPassword) {
                IconButton(onClick = { showPassword = false }) {
                    Icon(
                        imageVector = Icons.Default.Visibility,
                        contentDescription = "passwordVisible"
                    )
                }
            } else {
                IconButton(onClick = { showPassword = true }) {
                    Icon(
                        imageVector = Icons.Default.VisibilityOff,
                        contentDescription = "passwordInvisible"
                    )
                }
            }
        },
        visualTransformation = if (showPassword) VisualTransformation.None else PasswordVisualTransformation(),
        keyboardOptions = KeyboardOptions(keyboardType = KeyboardType.Password),
        singleLine = true,
        isError = isError,
        label = { Text(text = label) },
        placeholder = { Text(text = placeholder) },
        supportingText = { Text(text = supportingText) }
    )
}