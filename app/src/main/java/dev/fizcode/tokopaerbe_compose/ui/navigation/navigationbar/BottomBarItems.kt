package dev.fizcode.tokopaerbe_compose.ui.navigation.navigationbar

import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.rounded.Favorite
import androidx.compose.material.icons.rounded.Home
import androidx.compose.material.icons.rounded.ListAlt
import androidx.compose.material.icons.rounded.Store
import androidx.compose.ui.graphics.vector.ImageVector

/**
 * Created By FizCode on 1/21/2024.
 */

sealed class BottomBarItems(
    val route: String,
    val title: String,
    val icon: ImageVector
) {
    object Home : BottomBarItems(
        route = "HOME",
        title = "Home",
        icon = Icons.Rounded.Home
    )

    object Store : BottomBarItems(
        route = "STORE",
        title = "Store",
        icon = Icons.Rounded.Store
    )

    object Wishlist : BottomBarItems(
        route = "WISHLIST",
        title = "Wishlist",
        icon = Icons.Rounded.Favorite
    )

    object Transaction : BottomBarItems(
        route = "TRANSACTION",
        title = "Wishlist",
        icon = Icons.Rounded.ListAlt
    )
}