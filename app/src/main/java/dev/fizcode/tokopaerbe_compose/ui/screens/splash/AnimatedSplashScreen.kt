package dev.fizcode.tokopaerbe_compose.ui.screens.splash

import androidx.compose.animation.core.AnimationSpec
import androidx.compose.animation.core.animateDpAsState
import androidx.compose.animation.core.animateFloatAsState
import androidx.compose.animation.core.tween
import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.offset
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.alpha
import androidx.compose.ui.draw.clip
import androidx.compose.ui.draw.rotate
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.unit.Dp
import androidx.compose.ui.unit.dp
import androidx.hilt.navigation.compose.hiltViewModel
import androidx.navigation.NavHostController
import dev.fizcode.tokopaerbe_compose.R
import dev.fizcode.tokopaerbe_compose.ui.component.WhiteSystemBar
import dev.fizcode.tokopaerbe_compose.ui.navigation.navgraph.Graph
import kotlinx.coroutines.delay

/**
 * Created By FizCode on 1/19/2024.
 */

@Composable
fun AnimatedSplashScreen(navController: NavHostController) {

    // Change System Bar Background to be White
    WhiteSystemBar()

    SplashAnimation(navController = navController)
}

// Splash Image and box animation behind
@Composable
private fun Splash(
    alpha: Float,
    yellowDegree: Float,
    redDegree: Float,
    translate: Dp
) {
    Box(modifier = Modifier
        .fillMaxSize(),
        contentAlignment = Alignment.Center
    ) {

        // Green Box
        Box(modifier = Modifier
            .offset(y = -translate - 16.dp)
            .alpha(alpha)
            .clip(RoundedCornerShape(16.dp))
            .background(Color(0xFF2CA664))
            .size(height = 100.dp, width = 100.dp),
        )

        // Red Box
        Box(modifier = Modifier
            .offset(y = -translate, x = -translate + 8.dp)
            .rotate(redDegree)
            .alpha(alpha)
            .clip(RoundedCornerShape(16.dp))
            .background(Color(0xFFE75641))
            .size(height = 100.dp, width = 130.dp),
        )

        // Yellow Box
        Box(modifier = Modifier
            .offset(y = -translate, x = translate - 8.dp)
            .rotate(yellowDegree)
            .alpha(alpha)
            .clip(RoundedCornerShape(16.dp))
            .background(Color(0xFFFBC901))
            .size(height = 100.dp, width = 130.dp),
        )
        // Splash Image
        Image(
            modifier = Modifier
                .alpha(alpha)
                .size(140.dp),
            painter = painterResource(id = R.drawable.splash),
            contentDescription = "Splash"
        )
    }
}
@Composable
private fun SplashAnimation(navController: NavHostController) {

    var skipOnboarding by remember { mutableStateOf(false) }
    var signIn by remember { mutableStateOf(false) }

    // BindViewModel
    val splashViewModel: SplashViewModel = hiltViewModel()
    splashViewModel.onViewLoaded()

    val shouldSkipOnboarding = splashViewModel.shouldSkipOnBoarding.value
    val shouldSignIn = splashViewModel.shouldSignIn.value

    // State Value from ViewModel
    skipOnboarding = shouldSkipOnboarding
    signIn = shouldSignIn

    var startAnimation by remember { mutableStateOf(false) }

    val specDuration: AnimationSpec<Float> = tween(durationMillis = 1000)

    val alphaAnimation = animateFloatAsState(
        targetValue = if (startAnimation) 1f else 0f,
        animationSpec = specDuration,
        label = "Alpha Animation"
    )
    val translateY = animateDpAsState(
        targetValue = if (startAnimation) 16.dp else 0.dp,
        animationSpec = tween(durationMillis = 800),
        label = "Scaling Background"
    )

    val yellowRotateAnimation = animateFloatAsState(
        targetValue = if (startAnimation) -10f else 0f,
        animationSpec = specDuration,
        label = "Rotating Yellow"
    )
    val redRotateAnimation = animateFloatAsState(
        targetValue = if (startAnimation) 10f else 0f,
        animationSpec = specDuration,
        label = "Rotating Red"
    )

    // Starting the animation with delay 3 seconds and navigate to OnBoardingScreen
    LaunchedEffect(key1 = true){
        startAnimation = true
        delay(3000)
        navController.popBackStack()
        /*TODO: Navigate to OnBoarding or Home Screen*/
        if (skipOnboarding) {
            if (signIn) {
                navController.navigate(Graph.AUTHENTICATION)
            } else {
                navController.navigate(Graph.MAIN_MENU)
            }
        }
        else {
            navController.navigate(Graph.ONBOARDING)
        }
    }

    // Splash Screen UI
    Box(
        modifier = Modifier
            .fillMaxSize(),
        contentAlignment = Alignment.Center
    ) {
        Splash(
            alpha = alphaAnimation.value,
            yellowDegree = yellowRotateAnimation.value,
            redDegree = redRotateAnimation.value,
            translate = translateY.value
        )
    }
}