package dev.fizcode.tokopaerbe_compose.ui.screens.onboarding

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import dagger.hilt.android.lifecycle.HiltViewModel
import dev.fizcode.tokopaerbe_compose.common.Constant
import dev.fizcode.tokopaerbe_compose.repository.OnBoardingRepository
import kotlinx.coroutines.launch
import javax.inject.Inject

/**
 * Created By FizCode on 1/20/2024.
 */

@HiltViewModel
class OnBoardingViewModel @Inject constructor(
    private val onBoardingRepository: OnBoardingRepository
): ViewModel() {

    fun completeOnboarding() {
        viewModelScope.launch {
            onBoardingRepository.completeOnboarding(Constant.PrefValueSetter.ONBOARDING_STATUS)
        }
    }

}