package dev.fizcode.tokopaerbe_compose.ui.navigation.navgraph

import androidx.navigation.NavGraphBuilder
import androidx.navigation.NavHostController
import androidx.navigation.compose.composable
import androidx.navigation.navigation
import dev.fizcode.tokopaerbe_compose.ui.screens.splash.AnimatedSplashScreen

/**
 * Created By FizCode on 1/20/2024.
 */
fun NavGraphBuilder.splashNavGraph(navHostController: NavHostController) {
    navigation(
        route = Graph.SPLASH,
        startDestination = Splash.SplashScreen.route
    ) {
        composable(route = Splash.SplashScreen.route) {
            AnimatedSplashScreen(navController = navHostController)
        }
    }
}

sealed class Splash(val route: String) {
    object SplashScreen : Splash(route = "SPLASHSCREEN")
}