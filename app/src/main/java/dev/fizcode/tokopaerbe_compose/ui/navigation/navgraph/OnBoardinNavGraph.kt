package dev.fizcode.tokopaerbe_compose.ui.navigation.navgraph

import androidx.navigation.NavGraphBuilder
import androidx.navigation.NavHostController
import androidx.navigation.compose.composable
import androidx.navigation.navigation
import dev.fizcode.tokopaerbe_compose.ui.screens.onboarding.OnBoardingScreen

/**
 * Created By FizCode on 1/20/2024.
 */

fun NavGraphBuilder.onBoardingNavGraph(navController: NavHostController) {
    navigation(
        route = Graph.ONBOARDING,
        startDestination = OnBoardingScreen.OnBoarding.route
    ) {
        composable(route = OnBoardingScreen.OnBoarding.route) {
            OnBoardingScreen(navController = navController)
        }
    }
}

sealed class OnBoardingScreen(val route: String) {
    object OnBoarding: OnBoardingScreen(route = "ONBOARDING")
}