package dev.fizcode.tokopaerbe_compose.ui.component

import androidx.compose.foundation.isSystemInDarkTheme
import androidx.compose.material3.MaterialTheme
import androidx.compose.runtime.Composable
import com.google.accompanist.systemuicontroller.rememberSystemUiController

/**
 * Created By FizCode on 1/19/2024.
 */

@Composable
fun WhiteSystemBar() {
    val systemUiController = rememberSystemUiController()
    val darkTheme = isSystemInDarkTheme()

    if (darkTheme) {
        systemUiController.setSystemBarsColor(color = MaterialTheme.colorScheme.background,darkIcons = false)
        systemUiController.setNavigationBarColor(color = MaterialTheme.colorScheme.surface,darkIcons = false)
    } else {
        systemUiController.setStatusBarColor(color = MaterialTheme.colorScheme.background,darkIcons = true)
        systemUiController.setNavigationBarColor(color = MaterialTheme.colorScheme.surface,darkIcons = true)
    }
}