package dev.fizcode.tokopaerbe_compose.ui.screens.auth.signin

import androidx.lifecycle.ViewModel
import dagger.hilt.android.lifecycle.HiltViewModel
import dev.fizcode.tokopaerbe_compose.repository.AuthRepository
import javax.inject.Inject

/**
 * Created By FizCode on 1/20/2024.
 */

@HiltViewModel
class SignInViewModel @Inject constructor(
    private val authRepository: AuthRepository
): ViewModel() {



}