package dev.fizcode.tokopaerbe_compose.ui.screens.mainmenu.transaction

import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier

/**
 * Created By FizCode on 1/21/2024.
 */

@Composable
fun TransactionScreen() {
    Box(modifier = Modifier.fillMaxSize(), contentAlignment = Alignment.Center) {
        Text(
            style = MaterialTheme.typography.headlineLarge,
            text = "Transaction Screen"
        )
    }
}