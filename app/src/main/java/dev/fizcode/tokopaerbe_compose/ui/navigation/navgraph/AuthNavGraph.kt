package dev.fizcode.tokopaerbe_compose.ui.navigation.navgraph

import androidx.navigation.NavGraphBuilder
import androidx.navigation.NavHostController
import androidx.navigation.compose.composable
import androidx.navigation.navigation
import dev.fizcode.tokopaerbe_compose.ui.screens.auth.signin.SignInScreen
import dev.fizcode.tokopaerbe_compose.ui.screens.auth.signup.SignUpScreen

/**
 * Created By FizCode on 1/20/2024.
 */
fun NavGraphBuilder.authNavGraph(navController: NavHostController) {
    navigation(
        route  = Graph.AUTHENTICATION,
        startDestination = AuthScreen.SignIn.route
    ) {
        composable(route = AuthScreen.SignIn.route) {
            SignInScreen(navController = navController)
        }
        composable(route = AuthScreen.SignUp.route) {
            SignUpScreen(navController = navController)
        }
    }
}

sealed class AuthScreen(val route: String) {
    object SignIn : AuthScreen(route = "SIGN_IN")
    object SignUp : AuthScreen(route = "SIGN_UP")
}