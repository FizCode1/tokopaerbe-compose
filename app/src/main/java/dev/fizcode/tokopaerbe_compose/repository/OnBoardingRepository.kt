package dev.fizcode.tokopaerbe_compose.repository

import dev.fizcode.tokopaerbe_compose.datastore.SharedPreferences
import kotlinx.coroutines.flow.firstOrNull
import javax.inject.Inject

/**
 * Created By FizCode on 1/20/2024.
 */
class OnBoardingRepository @Inject constructor(
    private val sharedPreferences: SharedPreferences
) {
    suspend fun getOnboarding(): String? {
        return sharedPreferences.getOnBoarding().firstOrNull()
    }
    suspend fun completeOnboarding(value: String) {
        return sharedPreferences.setOnBoarding(value)
    }
    suspend fun clearOnboarding() {
        completeOnboarding("")
    }
}