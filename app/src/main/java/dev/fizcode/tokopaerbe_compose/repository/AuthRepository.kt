package dev.fizcode.tokopaerbe_compose.repository

import dev.fizcode.tokopaerbe_compose.datastore.SharedPreferences
import kotlinx.coroutines.flow.firstOrNull
import javax.inject.Inject

/**
 * Created By FizCode on 1/21/2024.
 */

class AuthRepository @Inject constructor(
    private val sharedPreferences: SharedPreferences
){
    suspend fun getToken(): String? {
        return sharedPreferences.getToken().firstOrNull()
    }
    suspend fun updateToken(value: String) {
        return sharedPreferences.setToken(value)
    }
    suspend fun clearToken() {
        updateToken("")
    }
}