package dev.fizcode.tokopaerbe_compose

import android.app.Application
import dagger.hilt.android.HiltAndroidApp

/**
 * Created By FizCode on 1/20/2024.
 */

@HiltAndroidApp
class Tokopaerbe: Application()