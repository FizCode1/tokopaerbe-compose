package dev.fizcode.tokopaerbe_compose.common

import androidx.datastore.preferences.core.stringPreferencesKey

/**
 * Created By FizCode on 1/20/2024.
 */
object Constant {
    object PrefDatastore {
        const val PREF_NAME = "Tokopaerbe"
        val ONBOARDING = stringPreferencesKey("ONBOARDING")
        val ACCESS_TOKEN = stringPreferencesKey("ACCESS_TOKEN")
    }

    object PrefValueSetter {
        const val ONBOARDING_STATUS = "COMPLETE"
    }

}