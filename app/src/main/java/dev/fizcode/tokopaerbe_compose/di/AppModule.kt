package dev.fizcode.tokopaerbe_compose.di

import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import dev.fizcode.tokopaerbe_compose.datastore.SharedPreferences
import dev.fizcode.tokopaerbe_compose.repository.OnBoardingRepository
import javax.inject.Singleton

/**
 * Created By FizCode on 1/20/2024.
 */

@Module
@InstallIn(SingletonComponent::class)
class AppModule {

    @Singleton
    @Provides
    fun provideOnboardingRepository(
        sharedPreferences: SharedPreferences
    ): OnBoardingRepository {
        return OnBoardingRepository(
            sharedPreferences = sharedPreferences
        )
    }
}