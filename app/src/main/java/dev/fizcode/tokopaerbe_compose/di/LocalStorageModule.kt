package dev.fizcode.tokopaerbe_compose.di

import android.content.Context
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent
import dev.fizcode.tokopaerbe_compose.datastore.SharedPreferences
import javax.inject.Singleton

/**
 * Created By FizCode on 1/20/2024.
 */

@Module
@InstallIn(SingletonComponent::class)
class LocalStorageModule {

    @Singleton
    @Provides
    fun provideOnboardingSharedPreferenceManager(@ApplicationContext context: Context)
    : SharedPreferences {
        return SharedPreferences(context = context)
    }
}